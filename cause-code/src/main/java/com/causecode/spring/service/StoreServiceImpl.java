package com.causecode.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.causecode.spring.model.Store;
import com.causecode.spring.dao.StoreDAO;

@Service("storeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StoreServiceImpl implements StoreService {

	@Autowired
	StoreDAO storeDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createStore(Store store) {
		storeDao.createStore(store);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public int deleteStore(int storeId) {
		return storeDao.deleteStore(storeId);
	}

	@Override
	@Transactional(readOnly = false)
	public void updateStore(Store store) {
		storeDao.updateStore(store);

	}

	@Override
	@Transactional(readOnly = false)
	public List<Store> getAllStores() {
		return storeDao.getAllStores();
	}

	@Override
	@Transactional(readOnly = false)
	public Store getStore(int storeId) {
		return storeDao.getStore(storeId);
	}
	
	@Override
	@Transactional(readOnly = false)
	public List<Store> fetchStore(int miles , String zipcode){
		return storeDao.fetchStore(miles, zipcode);
	}
}
