package com.causecode.spring.dao;

import java.util.List;

import com.causecode.spring.model.Store;

public interface StoreDAO {

	public abstract void createStore(Store store);

	public abstract int deleteStore(int storeId);

	public abstract void updateStore(Store store);

	public abstract List<Store> getAllStores();

	public abstract Store getStore(int storeId);
	
	public abstract List<Store> fetchStore(int miles , String zipcode);

}
