package com.causecode.spring.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.causecode.spring.exception.StoreException;
import com.causecode.spring.exception.StoreExceptionMessage;
import com.causecode.spring.model.Store;

@Repository("storeDao")
public class StoreDAOImpl implements StoreDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	private static final Logger logger = LoggerFactory
			.getLogger(StoreDAOImpl.class);

	@Override
	public void createStore(Store store) {
		try {
			sessionFactory.getCurrentSession().save(store);
		} catch (HibernateException e) {
			logger.error("Error while creating store " + e);
			throw new StoreException(StoreExceptionMessage.CREATE_EXCEPTION_MESSAGE.getMessage());
		}
	}

	@Override
	public int deleteStore(int storeId) {
		int isSuccessful; 
		isSuccessful = sessionFactory.getCurrentSession().createQuery("DELETE FROM Store s where s.storeId=:storeId").setInteger("storeId", storeId).executeUpdate();
		return isSuccessful;
	}

	@Override
	public void updateStore(Store store) {
			sessionFactory.getCurrentSession().update(store);
	}

	@Override
	public List<Store> getAllStores() {
		return sessionFactory.getCurrentSession().createCriteria(Store.class).list();
	}

	@Override
	public Store getStore(int storeId) {
		return (Store) sessionFactory.getCurrentSession().get(Store.class,storeId);
	}

	@Override
	public List<Store> fetchStore(int miles, String zipcode) {
		String query = "select latitude , longitude from zipcode where zipcode = :zipcode";
		Object[] objects = (Object[]) sessionFactory.getCurrentSession()
				.createSQLQuery(query).setString("zipcode", zipcode)
				.uniqueResult();
		if (objects != null) {
			return getStoresWithInRadius(miles, Float.valueOf(zipcode),((BigDecimal) objects[0]).floatValue(),((BigDecimal) objects[1]).floatValue());
		} else {
			return null;
		}
	}
	
	public List<Store> getStoresWithInRadius(float Radius, float ZipValue, float Latitude, float Longitude)  {
		String XprDistance =  "SQRT(POWER(("+Latitude+ "-z.latitude)*110.7,2)+POWER((" + Longitude + "-z.longitude)*75.6,2))";
		String sql = "SELECT s.* FROM store s left join zipcode z on s.zipcode = z.zipcode WHERE " + XprDistance + " <= ' " + Radius + "' ORDER BY s.store_id ASC";
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
		query.addEntity(Store.class);
		return query.list();
	}
}
