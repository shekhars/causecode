package com.causecode.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="zipcoe")
public class Zipcode {

	@Column(name= "countrycode")
	String countryCode;
	
	@Id
	@Column(name="zipcode")
	String zipcode;
	
	@Column(name="city")
	String city;
	
	@Column(name="city_aliases")
	String cityAliases;
	
	@Column(name="country")
	String country;
	
	@Column(name="country_fibs")
	String countryFips;

	@Column(name="state")
	String state;
	
	@Column(name="statecode")
	String stateCode;
	
	@Column(name="state_fips")
	String stateFips;
	
	@Column(name="timezone")
	String timeZone;
	
	@Column(name="daylightsaving")
	String daylightSaving;
	
	@Column(name="latitude")
	double latitude;
	
	@Column(name="longitude")
	double longitude;
	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityAliases() {
		return cityAliases;
	}

	public void setCityAliases(String cityAliases) {
		this.cityAliases = cityAliases;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryFips() {
		return countryFips;
	}

	public void setCountryFips(String countryFips) {
		this.countryFips = countryFips;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateFips() {
		return stateFips;
	}

	public void setStateFips(String stateFips) {
		this.stateFips = stateFips;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getDaylightSaving() {
		return daylightSaving;
	}

	public void setDaylightSaving(String daylightSaving) {
		this.daylightSaving = daylightSaving;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
