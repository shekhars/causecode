package com.causecode.spring.controller;

public class StoreURIConstants {

	public static final String GET_STORE = "/rest/store/{id}";
	public static final String GET_ALL_STORE = "/rest/stores";
	public static final String CREATE_STORE = "/rest/store/create";
	public static final String UPDATE_STORE = "/rest/store/update";
	public static final String DELETE_STORE = "/rest/store/delete/{id}";
	public static final String FETCH_STORE = "/rest/store/find/{miles}/{zipcode}";
}
