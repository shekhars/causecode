package com.causecode.spring.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.causecode.spring.exception.StoreException;
import com.causecode.spring.exception.StoreExceptionMessage;
import com.causecode.spring.model.Store;
import com.causecode.spring.service.StoreService;
import com.causecode.spring.validation.Validation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Handles requests for the Store service.
 */
@Controller
public class StoreController {

	private static final Logger logger = LoggerFactory
			.getLogger(StoreController.class);

	@Autowired
	StoreService storeService;
	
	@RequestMapping(value="/")
	public String showHomePage(){
		return "home";
	}
	
	@RequestMapping(value = StoreURIConstants.GET_STORE, method = RequestMethod.GET)
	public @ResponseBody Store getStore(@PathVariable("id") int storeId) {
		logger.info("Start getStore. ID=" + storeId);
		Validation.notEmpty(storeId);
		Store store = storeService.getStore(storeId);
		if (store == null) {
			logger.error("No Store is found with id " + storeId);
			throw new StoreException(
					StoreExceptionMessage.STORE_NOT_FOUND_MESSAGE.getMessage());
		}
		return store;
	}

	@RequestMapping(value = StoreURIConstants.GET_ALL_STORE, method = RequestMethod.GET)
	public @ResponseBody List<Store> getAllStores() {
		logger.info("Start getAllStores.");
		List<Store> stores = storeService.getAllStores();
		if (stores == null || stores.size() == 0) {
			logger.error("No Store is found");
			throw new StoreException(
					StoreExceptionMessage.STORE_NOT_FOUND_MESSAGE.getMessage());
		}
		return stores;
	}

	@RequestMapping(value = StoreURIConstants.CREATE_STORE, method = RequestMethod.POST)
	public @ResponseBody Store createStore(@RequestBody Store store) {
		logger.info("Start createStore.");
		Validation.notEmpty(store);
		storeService.createStore(store);
		return store;
	}

	@RequestMapping(value = StoreURIConstants.DELETE_STORE, method = RequestMethod.DELETE)
	public @ResponseBody void deleteStore(@PathVariable("id") int storeId) {
		logger.info("Start deleteStore.");
		Validation.notEmpty(storeId);
		int isSuccessful = storeService.deleteStore(storeId);
		if (isSuccessful == 0) {
			logger.error("Error while deleting store with store id " + storeId);
			throw new StoreException(
					StoreExceptionMessage.DELETE_EXCEPTION_MESSAGE.getMessage());
		}
	}
	
	@RequestMapping(value = StoreURIConstants.UPDATE_STORE, method = RequestMethod.PUT)
	public @ResponseBody Store updateStore(@RequestBody Store store) {
		logger.info("Start updateStore.");
		try {
			Validation.notEmpty(store.getStoreId());
			Validation.notEmpty(store);
			storeService.updateStore(store);
		} catch (Exception e) {
			logger.error("Error while updating store " + e);
			throw new StoreException(
					StoreExceptionMessage.UPDATE_EXCEPTION_MESSAGE.getMessage());
		}
		return store;
	}
	
	
	@RequestMapping(value = StoreURIConstants.FETCH_STORE, method = RequestMethod.GET)
	public @ResponseBody List<Store> fetchStore(@PathVariable("miles") int miles , @PathVariable("zipcode") String zipcode) {
		logger.info("Finding a store within" + miles + " miles of a USA zipcode " + zipcode);
		Validation.notEmpty(zipcode);
		List<Store> stores = storeService.fetchStore(miles, zipcode);
		if (stores == null || stores.size() == 0) {
			logger.error("No Store is found");
			throw new StoreException(
					StoreExceptionMessage.STORE_NOT_FOUND_MESSAGE.getMessage());
		}
		return stores;
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public String handleException(Exception e) {
		String s = "{}";
		ObjectMapper mapper = new ObjectMapper();
		try {
			s = mapper.writeValueAsString(e.getMessage());
			return s;
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		return s;
	}
}
