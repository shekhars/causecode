package com.causecode.spring.validation;

import com.causecode.spring.exception.StoreException;
import com.causecode.spring.model.Store;

public class Validation {

	public static void notEmpty(Store o)
	{
		if( o.getStoreName() == null || o.getStoreName().isEmpty() )
			throw new StoreException("store name field is empty or null");
		
		if( o.getLocation() == null || o.getLocation().isEmpty() )
			throw new StoreException("location field is empty or null");
		
		if( o.getZipcode() == 0 )
			throw new StoreException("zip code field is empty or null");
		
	}
	
	public static void notEmpty(Integer o)
	{
		if( o == 0 )
			throw new StoreException("store id is empty");
	}
	
	public static void notEmpty(String o)
	{
		if( o == null || o.isEmpty())
			throw new StoreException("zipcode id is empty");
	}
}
