package com.causecode.spring.exception;

public enum StoreExceptionMessage {
	
	CREATE_EXCEPTION_MESSAGE("Error while creating store, check if any parameter is null"),
	UPDATE_EXCEPTION_MESSAGE("Error while updating store, check if any parameter is null or store is not found"),
	DELETE_EXCEPTION_MESSAGE("Error while deletion store, check if store id is present"),
	STORE_NOT_FOUND_MESSAGE("No Store is found");
	
	String message;  
	StoreExceptionMessage(String message)
	{
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
