package com.causecode.spring.exception;

public class StoreException extends RuntimeException {

	String message;
	public StoreException(String message) {
		super(message);
		this.message = message;
	}
}
