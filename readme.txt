===========================================

This project supports following operations

1. Create a Store

2. Update a Store

3. Delete a Store

4. Fetch a store with a particular id

5. Fetch all the stores

6. Find a store within "x" miles of a USA zipcode



1. Create a Store


For creating a store, the following REST API Url will be used

POST : http://localhost:9090/causecode/rest/store/create


and provide in the body like following 

{
  
"storeName": "Reliance Fresh",
  
"location": "Baner Road ,Pune",
"zipcode": 90004

}



2. Update a Store


For updating a store, the following REST API Url will be used

PUT: http://localhost:9090/causecode/rest/store/update 
and provide in the body like following 

{
"storeId": 9,
    
"storeName": "Reliance Fresh",
    
"location": "Pashan Sus Road",
    
"zipcode": 90002

}



3. Delete a Store


For deleting a store, the following REST API Url will be used

PUT: http://localhost:9090/causecode/rest/store/delete/8



4. Fetch a store with a particular id


For fetching a store with a particular id, the following REST API Url will be used

GET: http://localhost:9090/causecode/rest/store/4



5. Fetch all the stores


For fetching all the stores, the following REST API Url will be used

GET: http://localhost:9090/causecode/rest/stores



6. Find a store within "x" miles of a USA zipcode


For fetching all the stores within "x" miles of a USA zipcode, the following REST API Url will be used

GET: http://localhost:9090/causecode/rest/store/find/10/90001


the following sample output will be returned

[
  
{
    
"storeId": 9,

"storeName": "Reliance Fresh",
    
"location": "Pashan Sus Road",
    
"zipcode": 90002
  
}

]


====================================================================


Steps to create environment for running project

====================================================================

1. Run MySQL database on 3307 port or change port in servlet-context.xml(/causecode/src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml) file, change configuration according to your database in this file


2. Create database causecode and create following two tables



a. CREATE TABLE `store` 
(
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  
`store_name` varchar(100) NOT NULL,
  
`location` varchar(40) NOT NULL,
  
`zipcode` int(6) NOT NULL,
  
PRIMARY KEY (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;



b. USE causecode

CREATE TABLE zipcode
(
countrycode	varchar(2),

zipcode 		varchar(5),

city		varchar(64),

city-aliases	varchar(5000),

county varchar(64),

county-fips	varchar(3),

state		varchar(48),

statecode	varchar(2),

state-fips	varchar(2),

timezone	varchar(10),

daylightsaving	varchar(1),

latitude	decimal(13, 9),

longitude	decimal(13, 9)
)



USE causecode
 


LOAD DATA INFILE 'C:\\US.txt' \\this file has been commited in the project for storing US zipcode

INTO TABLE zipcode

FIELDS TERMINATED BY ';'

LINES TERMINATED BY '\n'
IGNORE 1 LINES;



3.Import project into eclipse or any other IDEs and add tomacat-apache server and right click on the project and click on 'Run On Server'
